+++
title = "Дніпрожидівський шизопатріотизм"
slug = "dniprozhydivskyj-shyzopatriotyzm"
author = "Пальма3000"
date = "2023-03-18"
tags = ["фото", "Дніпрожидівськ", "шизопатріоти"]
description = "Фотографії дніпрожидовських шизопатріотичних будок"
showFullContent = false
readingTime = false
hideComments = false
+++

## Шаурнація 

![Будка з шаурмою "Шаурнація"](shaurnatsia.jpg)

## Пивний патріот

![Генделик "Пивний патріот"](pyvnyj_patriot.jpg)