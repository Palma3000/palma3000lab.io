+++
title = "Небінарна окраса мого двору в літній дощовий день"
slug = "nebinarna-okrasa-moho-dvoru-v-litnij-doschovyj-den"
author = "Пальма3000"
date = "2023-08-10"
tags = ["фото", "Дніпрожидівськ", "малюнки"]
description = "Фотографії виноградного равлика та мій начерк."
showFullContent = false
readingTime = false
hideComments = false
+++

[Виноградний равлик](https://uk.wikipedia.org/wiki/Helix_pomatia) (**лат.** Helix Pomatia) виліз на дорогу під час дощу та радує мене своєю красою.

![Фотографія виноградного равлика](nonbinary1.jpg)

![Фотографія виноградного равлика](nonbinary2.jpg)

![Фотографія виноградного равлика](nonbinary3.jpg)

## Мій начерк виноградного равлика

![Начерк виноградного равлика](nonbinary4.jpg)
