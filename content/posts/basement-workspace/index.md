+++
title = "Моє робоче місце в підвалі"
slug = "bomboskhovysche-v-pidvali"
date = "2022-10-15"
author = "Пальма3000"
tags = ["фото", "робоче місце", "бомбосховище"]
+++

Моє робоче місце в підвалі, де я сидю під час бомбардувань Дніпрожидівська.

![Little dark age](basement-workspace.jpg)