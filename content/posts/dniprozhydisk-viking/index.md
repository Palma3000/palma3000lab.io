+++
title = "Оздоблений рунічними написами вхід до садиби дніпрожидівського вікінга"
slug = "ozdoblenyj-runichnymy-napysamy-vhid-do-sadyby-dniprozhydivskoho-vikinha"
author = "Пальма3000"
date = "2022-10-03"
tags = ["фото", "Дніпрожидівськ", "руни"]
description = ""
showFullContent = false
readingTime = false
hideComments = false
+++

![Фотографія оздобленого рунічними написами входу до садиби дніпрожидівського вікінга](dniprozhydivskyj_vikynh.jpg)
