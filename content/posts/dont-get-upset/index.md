+++
title = "Не розстраюйся"
slug = "ne-rozstrajujsia"
author = "Пальма3000"
date = "2023-06-17"
tags = ["відео", "моя творчість", "мотиваційне", "для важливих переговорів"]
showFullContent = false
readingTime = false
hideComments = false
+++

### Готичне "не розстраюйся"

{{< video src="hotychne-ne-rozstrajujsia" >}}

### Вінтажно-програмерське "не розстраюйся"

{{< video src="vintazhno-prohramerske-ne-rozstrajujsia" >}}

### Хвойне "не розстраюйся"

{{< video src="khvojne-ne-rozstrajujsia" >}}