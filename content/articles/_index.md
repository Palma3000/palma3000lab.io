+++
title = "Статті"
layout = "single"
+++

* [Обчислення математичної задачі на радянському програмованому калькуляторі зі зворотнім польським записом. Знаходження віддалі між точками на площині](obchyslennia-na-radianskomy-rpn-kalkulatori-1)
* [Чим проміжне представлення LLVM IR краще ніж мова асемблера](chym_promizhne_predstavlennia_krasche_nizh_mova_asemblera) (переклад)
* [\[Чернетка\] Створення ігор з LÖVE від *sheepolution*](/how2love) (переклад)


