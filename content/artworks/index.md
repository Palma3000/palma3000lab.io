+++
title = "Мої малюнки"
+++

### Малюнок олівцем "Заклик до Ромашки не шизіти"

![Малюнок олівцем "Заклик до Ромашки не шизіти"](romashka-dont-be-crazy/romashka-dont-be-crazy.jpg)

### Малюнок олівцем "Заклик до падаванів вчити програмування"

![Малюнок олівцем "Заклик до падаванів вчити програмування"](padavans-learn-coding/padavans-learn-coding.jpg)

### Малюнок олівцем "Еротичні хвантазії VHS"

![Малюнок олівцем "Еротичні хвантазії VHS"](erotic-fantasy-vhs/erotic-fantasy-vhs.jpg)

### Малюнок олівцем "Сумний Гітлер загляда у віконце"

![Малюнок олівцем "Сумний Гітлер загляда у віконце"](sad-hitler-looks-in-the-window/sad-hitler-looks-in-the-window.jpg)

### Малюнок олівцем "E ВІДДАЙ САЛО"

![Малюнок олівцем "Е ВІДДАЙ САЛО"](give-the-lard/give-the-lard.jpg)

### Малюнок олівцем "Побажання одужання Ромашці"

![Малюнок олівцем "Побажання одужання Ромашці"](romashka-feel-better/romashka-feel-better.jpg)

### Акварельний малюнок "Самотня квітка в місячну ніч"

![Акварельний малюнок "Самотня квітка в місячну ніч"](a-lonely-flower-on-a-moonlight-night/a-lonely-flower-on-a-moonlight-night.jpg)

### Малюнок олівцем "Abyss abyssum invocat"

![Малюнок олівцем "abyss-abyssum-invocat"](abyss-abyssym-invocat/abyss-abyssym-invocat.jpg)

### Малюнок гелевою ручкою "Ars longa, vita brevis est"

![Малюнок гелевою ручкою "Ars longa, vita brevis est"](ars-longa-vita-brevis-est/ars-longa-vita-brevis-est.jpg)